function checkLeapYear(year1,year2) {
    let array = []

    for(let year = year1; year<=year2; year++){
    if ((0 == year % 4) && (0 != year % 100) || (0 == year % 400)) {
        array.push(year)
      } 
    }
    console.log(array)
}

const year1 = 1900
const year2 = 2020

checkLeapYear(year1,year2);
