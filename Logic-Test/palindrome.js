function palindrome(str) {
    var re = /[\W_]/g;
    var lowStr = str.toLowerCase().replace(re, "");
    var reversStr = lowStr.split("").reverse().join("");
    return reversStr === lowStr;
  }
  console.log(palindrome("Radar"));
  console.log(palindrome("Malam"));
  console.log(palindrome("Kasur ini rusak"));
  console.log(palindrome("Ibu Ratna antar ubi"));
  console.log(palindrome("Malas"));
  console.log(palindrome("Makan nasi goreng"));
  console.log(palindrome("Balonku ada lima"));