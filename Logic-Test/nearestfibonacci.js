function nearestFibonacci(num)
{
    // Base Case
    if (num == 0) {
        // document.write(0);
        console.log(0)
        return;
    }
 
    // Initialize the first & second
    // terms of the Fibonacci series
    let first = 0, second = 1;
 
    // Store the third term
    let third = first + second;
 
    // Iterate until the third term
    // is less than or equal to num
    while (third <= num) {
 
        // Update the first
        first = second;
        // console.log(first)
 
        // Update the second
        second = third;
        // console.log(second)
 
        // Update the third
        third = first + second;
        // console.log(third)
    }
 
    // Store the Fibonacci number
    // having smaller difference with N
    let ans = (Math.abs(third - num)
               >= Math.abs(second - num))
                  ? second
                  : third;
    
    // console.log(ans);
    console.log("Nearest Fibonacci")
    console.log(ans - num);
}
 
// Driver Code


var nomor = [1,2,3];
var total = 0;

for(i = 0; i <nomor.length; i++){
    total += nomor[i];
 }

//  console.log(total);
// let N = 6;
nearestFibonacci(total);
 