const fs = require('fs');

const loadNote = () => {
    try {
        const dataBuffer = fs.readFileSync("data.json");
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch (error) {
        return [];
    }
};

console.log("****************************************************************************")
console.log("Find items in the Meeting Room.")
function findItems() {
    const data = loadNote();
    let str = []
    data.forEach((items) => {
        if (items.placement.name === "Meeting Room")
            str.push(items)
    })
    return str
}
console.log(findItems())
console.log("****************************************************************************")


console.log("****************************************************************************")
console.log("Find all electronic devices.")
function findElectronicalItems() {
    const data = loadNote();
    let str = []
    data.forEach((items) => {
        if (items.type == 'electronic') {
            str.push(items)
        }
    })
    return str
}
console.log(findElectronicalItems());
console.log("****************************************************************************")


console.log("****************************************************************************")
console.log("Find all the furniture.")
function findFurnitureItems() {
  const data = loadNote();
  let str = []
  data.forEach((items) => {
      if (items.type == 'furniture') {
          str.push(items)
      }
  })
  return str
}
console.log(findFurnitureItems());
console.log("****************************************************************************")


console.log("****************************************************************************")
console.log("Find all items were purchased on 16 Januari 2020.")
//mengubah detik menjadi tanggal, bulan dan tahun.
// var date = new Date(1578672242 * 1000)
// console.log(date.toISOString().substring(0,10))
//atau versi 2
// var myDate = "16-01-2020";
// myDate = myDate.split("-");
// var newDate = new Date( myDate[2], myDate[1] - 1, myDate[0]);
// console.log(newDate.getTime());

function purchasedOn(date){
  const data = loadNote();
  let str = []
  data.forEach((items) => {
      var purchased_at = new Date(items.purchased_at * 1000).toISOString().substring(0,10)
      if (purchased_at === date) {
          str.push(items)
      }
  })
  return str
}
console.log(purchasedOn("2020-01-16"));
console.log("****************************************************************************")


console.log("****************************************************************************")
console.log("Find all items with brown color.")
function findBrownItems() {
  const data = loadNote();
  let str = []
  data.forEach((items) => {
      items.tags.forEach((e) =>{
        if (e == 'brown') {
          str.push(items)
      }
      })
  })
  return str
}
console.log(findBrownItems());
console.log("****************************************************************************")